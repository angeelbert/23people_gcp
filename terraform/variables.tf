variable "name" {
  default = "demo-cluster"
}

variable "project" {
  default = "	tidy-rig-283500"
}

variable "location" {
  default = "us-east1-b"
}

variable "initial_node_count" {
  default = 3
}

variable "machine_type" {
  default = "n1-standard-1"
}
